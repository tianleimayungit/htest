import App from './App'

import VueI18n from "vue-i18n";
import utils from '@/config/helper'
Vue.use(VueI18n);
Vue.use(utils);
const i18n = new VueI18n({
    locale: uni.getStorageSync('lang') == (undefined || "en-US" || null) ? "zh" : uni.getStorageSync('lang'),
    messages: {
        ft: require("./static/lang/jt.json"),
        en: require("./static/lang/en.json"),
		ko: require("./static/lang/ko.json"),
		jp: require("./static/lang/ja.json"),
		zh: require("./static/lang/zh.json"),
    }
});

if(i18n.locale=='en-US'){
	i18n.locale='zh'
	localStorage.setItem('lang','zh');
}
Vue.prototype._i18n = i18n

Vue.prototype.$utils=utils;
// #ifndef VUE3 
import Vue from 'vue'

import uView from "./uni_modules/uview-ui";
Vue.use(uView);

import './uni.promisify.adaptor'
Vue.config.productionTip = false;

App.mpType = 'app'
const app = new Vue({
  i18n,
  ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif